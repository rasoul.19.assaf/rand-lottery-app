import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:lottery_app/games/brick_breaker/brick_breaker.dart';
import 'package:lottery_app/games/coin_flip_screen/coin_flip_screen.dart';
import 'package:lottery_app/games/flappy_bird_screen/FlappyBirdScreen.dart';
import 'package:lottery_app/games/random_number_screen/random_number_screen.dart';
import 'package:lottery_app/games/stop_image_screen.dart';
import 'package:lottery_app/questionnaire_screen/questionnaire_screen.dart';

import '../data.dart';
import '../games/flip_cards_screen/flip_cards_screen.dart';
import '../games/snake_game/snake_screen.dart';
import '../games/tetris/tetris_screen.dart';
import '../games/tic_tac_toe/ai/ai.dart';
import '../games/tic_tac_toe/single_player_game.dart';

class MainScreen extends StatefulWidget {
  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  Widget mainWidget = CoinFlipScreen();

  @override
  Widget build(BuildContext context) {
    // print(MediaQuery.of(context).size);
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.blue,
          body: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Stack(
                  children: [
                    /*  Container(
                      height: 140,
                      // margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(18),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Text("00:00:14"),
                              SizedBox(
                                width: 10,
                              ),
                              Text("2000"),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: LinearProgressIndicator(
                                  value: 0.01,
                                  backgroundColor: Colors.yellow,
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Icon(
                                Icons.card_giftcard,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text("00:00:14"),
                              SizedBox(
                                width: 10,
                              ),
                              Text("2000"),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: LinearProgressIndicator(
                                  value: 0.05,
                                  backgroundColor: Colors.yellow,
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Icon(
                                Icons.card_giftcard,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text("00:00:14"),
                              SizedBox(
                                width: 10,
                              ),
                              Text("2000"),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: LinearProgressIndicator(
                                  value: 0.05,
                                  backgroundColor: Colors.yellow,
                                  color: Colors.grey,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Icon(
                                Icons.card_giftcard,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),*/
                    Container(
                      margin: EdgeInsets.all(18),
                      height: 50,
                      child:
                          ListView(scrollDirection: Axis.horizontal, children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => QuestionaireScreen(),
                              ),
                            );
                          },
                          child: Container(
                            height: 50,
                            color: Colors.red,
                            width: 50,
                            child: Icon(Icons.question_mark),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BrickBreaker(),
                              ),
                            );
                          },
                          child: Container(
                            height: 50,
                            color: Colors.cyan,
                            width: 50,
                            child: Icon(Icons.crop_square),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            log("Navigating to flappy bird game");
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => FlappyBirdScreen(),
                              ),
                            );
                          },
                          child: Container(
                            height: 50,
                            color: Colors.amberAccent,
                            width: 50,
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Image.asset(
                                  'assets/images/flappy_bird/flappybird.png'),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              mainWidget = RandomNumberScreen();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.purple,
                            child: Icon(Icons.numbers),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              mainWidget = StopImageScreen();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.green,
                            child: Icon(Icons.animation),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => TetrisScreen(),
                                ),
                              );
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.pink,
                            child: Icon(Icons.videogame_asset),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              mainWidget = SnakeScreen();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.greenAccent,
                            child: Icon(Icons.gesture),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            print("dfs");
                            setState(() {
                              mainWidget = SinglePlayerGame(
                                  difficulty: GameDifficulty.Easy);
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.orange,
                            child: Icon(Icons.grid_on),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              mainWidget = FlipCardsScreen();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.blue,
                            child: Icon(Icons.quiz),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              mainWidget = CoinFlipScreen();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            color: Colors.yellow,
                            child: Icon(Icons.attach_money),
                          ),
                        ),
                      ]),
                    ),
                  ],
                ),
                Expanded(child: mainWidget),
              ],
            ),
          )),
    );
  }
}
