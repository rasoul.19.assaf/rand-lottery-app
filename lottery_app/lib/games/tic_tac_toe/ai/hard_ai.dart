import 'dart:math';

import 'package:lottery_app/games/tic_tac_toe/ai/ai.dart';
import 'package:lottery_app/games/tic_tac_toe/single_player_game.dart';

class HardAi extends AI {
  GameState aiPlayer;

  HardAi(this.aiPlayer);

  @override
  List<int> getMove(List<List<GameState>> board, int turns) {
    var result = findBestMove(board, aiPlayer);
    printBoard(board);
    print(result);

    return result;
  }

  // This will return the best possible move for the player
  List<int> findBestMove(List<List<GameState>> board, GameState player) {
    int bestVal = -1000;
    List<int> bestMove = [-1, -1];
    // Traverse all cells, evaluate minimax function for all empty cells. And return the cell with optimal value.
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        // Check if cell is empty
        if (board[i][j] == GameState.B) {
          // Make the move
          board[i][j] = player;

          // compute evaluation function for this move.
          int moveVal = minMax(board, 0, false, -1000, 1000);
          // Undo the move
          board[i][j] = GameState.B;

          // If the value of the current move is more than the best value, then update best
          if (moveVal > bestVal) {
            bestMove = [i, j];
            bestVal = moveVal;
          }
        }
      }
    }

    print("The value of the best Move is :" + bestVal.toString());
    return bestMove;
  }

  int minMax(
      List<List<GameState>> board, int depth, bool isMax, int alpha, int beta) {
    int score = evaluate(board, aiPlayer);
    // print("Score: " + score.toString());

    // If Maximizer has won the game return his/her evaluated score
    if (score == 10) {
      return score;
    }

    // If Minimizer has won the game return his/her evaluated score
    if (score == -10) {
      return score;
    }
/*
    if (depth == 5) {
      return score;
    }*/

    // If there are no more moves and no winner then it is a tie
    if (!isMovesLeft(board)) {
      return 0;
    }

    int best;
    // If this maximizer's move
    if (isMax) {
      best = -1000;

      // Traverse all cells
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
          // Check if cell is empty

          if (board[i][j] == GameState.B) {
            // Make the move
            board[i][j] = GameState.O;
            // Call minimax recursively and choose the maximum value
            best = max(
              best,
              minMax(
                board,
                depth + 1,
                false,
                alpha,
                beta,
              ),
            );
            alpha = max(alpha, best);
            // Undo the move
            board[i][j] = GameState.B;
            if (beta <= alpha) {
              return best - depth; // TODO: -depth
            }
          }
        }
      }
      return best - depth; // TODO: -depth
    } else {
      best = 1000;
      // Traverse all cells
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
          // Check if cell is empty
          if (board[i][j] == GameState.B) {
            // Make the move
            board[i][j] = GameState.X;

            // Call minimax recursively and choose the minimum value
            best = min(
              best,
              minMax(
                board,
                depth + 1,
                true,
                alpha,
                beta,
              ),
            );

            beta = min(beta, best);
            // Undo the move
            board[i][j] = GameState.B;
            if (beta <= alpha) {
              return best + depth; //TODO: + depth
            }
          }
        }
      }
      return best + depth; //TODO: + depth

    }
  }

  bool isMovesLeft(List<List<GameState>> board) {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (board[i][j] == GameState.B) {
          return true;
        }
      }
    }
    return false;
  }

  int evaluate(List<List<GameState>> boardState, GameState player) {
    /* print("Evaluating");
    printBoard(boardState);*/
    // Checking for Rows for X or O victory.
    for (int i = 0; i < 3; i++) {
      if (boardState[i][0] == boardState[i][1] &&
          boardState[i][1] == boardState[i][2]) {
        if (boardState[i][0] == player) {
          return 10;
        } else if (boardState[i][0] != GameState.B) {
          return -10;
        }
      }
    }
    // Checking for Rows for X or O victory.
    for (int i = 0; i < 3; i++) {
      if (boardState[i][0] == boardState[i][1] &&
          boardState[i][1] == boardState[i][2]) {
        if (boardState[i][0] == player) {
          return 10;
        } else if (boardState[i][0] != GameState.B) {
          return -10;
        }
      }
    }

    // Checking for Columns for X or O victory.
    for (int i = 0; i < 3; i++) {
      if (boardState[0][i] == boardState[1][i] &&
          boardState[1][i] == boardState[2][i]) {
        if (boardState[0][i] == player) {
          return 10;
        } else if (boardState[0][i] != GameState.B) {
          return -10;
        }
      }
    }
    // Checking for Diagonals for X or O victory.
    if (boardState[0][0] == boardState[1][1] &&
        boardState[1][1] == boardState[2][2]) {
      if (boardState[0][0] == player) {
        return 10;
      } else if (boardState[0][0] != GameState.B) {
        return -10;
      }
    }

    if (boardState[0][2] == boardState[1][1] &&
        boardState[1][1] == boardState[2][0]) {
      if (boardState[0][2] == player) {
        return 10;
      } else if (boardState[0][2] != GameState.B) {
        return -10;
      }
    }
    // # Else if none of them have won then return 0
    return 0;
  }

  GameState getOpponent(GameState player) {
    if (player == GameState.B) {
      return GameState.B;
    }
    if (player == GameState.X) {
      return GameState.O;
    }
    return GameState.X;
  }
}

void printBoard(List<List<GameState>> boardState) {
  for (int i = 0; i < 3; i++) {
    print(boardState[i][0].name +
        " " +
        boardState[i][1].name +
        " " +
        boardState[i][2].name);
  }
}
