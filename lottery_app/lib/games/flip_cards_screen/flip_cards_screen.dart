import 'dart:async';
import 'dart:math';

import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:lottery_app/games/tic_tac_toe/themes.dart';

class FlipCardsScreen extends StatefulWidget {
  @override
  State<FlipCardsScreen> createState() => _FlipCardsScreenState();
}

class _FlipCardsScreenState extends State<FlipCardsScreen> {
  int score = 0;
  bool? win;
  late int winPng;
  late int position1;
  late int position2;
  List<int> randomList = [];
  List<int> openedCards = [];
  List<FlipCardController> flipCardControllers = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 6; i++) {
      flipCardControllers.add(FlipCardController());
    }
    initCards();
  }

  void initCards() {
    randomList.clear();
    winPng = getRandomInteger(8);
    position1 = getRandomInteger(6);
    do {
      position2 = getRandomInteger(6);
    } while (position1 == position2);
    for (int i = 0; i < 6; i++) {
      int png = getRandomInteger(8);
      while (png == winPng || randomList.contains(png)) {
        png = getRandomInteger(8);
      }
      randomList.add(png);
    }
    randomList.shuffle();
    randomList[position1] = winPng;
    randomList[position2] = winPng;
  }

  getRandomInteger(int max) {
    return Random().nextInt(max);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        win != null
            ? Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  win! ? "You Win! " : "You Lose! ",
                  style: TextStyle(
                    fontSize: 50,
                    color: Colors.yellowAccent,
                  ),
                ),
              )
            : Table(
                children: [
                  TableRow(
                    children: [
                      getTableCell(0),
                      getTableCell(1),
                      getTableCell(2),
                    ],
                  ),
                  TableRow(
                    children: [
                      getTableCell(3),
                      getTableCell(4),
                      getTableCell(5),
                    ],
                  ),
                ],
              ),
        bottomBar,
        /*Padding(
                padding: const EdgeInsets.all(20.0),
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      initCards();
                      score = 0;
                      openedCards = [];
                    });
                  },
                  child: Icon(
                    Icons.refresh,
                    size: 30,
                  ),
                ),
              ),*/
      ],
    );
  }

  Widget get bottomBar => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FloatingActionButton(
            heroTag: 'reset',
            child: Icon(Icons.cached),
            backgroundColor: accentColor,
            mini: true,
            onPressed: () {
              initCards();
              score = 0;
              try {
                int length = openedCards.length;
                for (int i = 0; i < length; i++) {
                  if (!flipCardControllers[openedCards[i]].state!.isFront) {
                    flipCardControllers[openedCards[i]].toggleCard();
                  }
                  length > openedCards.length ? i-- : null;
                }
              } catch (e) {}
              openedCards.clear();
              setState(() {
                openedCards;
                score;
                randomList;
                openedCards;
                win = null;
              });
            },
          ),
        ],
      );

  getTableCell(int i) {
    return TableCell(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: FlipCard(
          controller: flipCardControllers[i],
          flipOnTouch: (openedCards.length == 2 && !openedCards.contains(i))
              ? false
              : true,
          onFlipDone: (val) {
            print(val);
            print('enters2');

            if (!val) {
              setState(() {
                openedCards.remove(i);
              });
            } else if (val) {
              setState(() {
                openedCards.add(i);
              });
            }
            checkWin();
          },
          fill: Fill.none,
          front: SizedBox(
            height: 150,
            child: Card(
              elevation: 2,
              child: Icon(
                Icons.quiz,
              ),
            ),
          ),
          back: SizedBox(
            height: 150,
            child: Card(
              color: Colors.transparent,
              elevation: 0,
              child: Image(
                image: AssetImage('assets/images/${randomList[i]}.png'),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void checkWin() {
    print(openedCards);
    print("p1");
    print(position1);
    print("p2");
    print(position2);

    if (openedCards.length == 2) {
      if (openedCards.contains(position1) && openedCards.contains(position2)) {
        Timer(Duration(seconds: 1), () {
          setState(() {
            win = true;
          });
        });
      } else {
        Timer(Duration(seconds: 1), () {
          setState(() {
            win = false;
          });
        });
      }
    }
  }
}
