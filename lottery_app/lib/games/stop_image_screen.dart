import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lottery_app/games/tic_tac_toe/themes.dart';

class StopImageScreen extends StatefulWidget {
  @override
  State<StopImageScreen> createState() => _StopImageScreenState();
}

class _StopImageScreenState extends State<StopImageScreen>
    with SingleTickerProviderStateMixin {
  bool? win;

  late final AnimationController _controller;
  late final Animation<double> myAnimationController;

  bool showResult = false;
  String result = '';

  @override
  void initState() {
    super.initState();
    initializeAnimation();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // createTranslateAnimation();
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            // myAnimationController
            _controller.stop();
            print(500 * _controller.value - 250 - 0);
            double offset = 500 * _controller.value - 250 - 0;
            if (offset < 10 && offset > -10) {
              _controller.value = 0.5;
              win = true;
              result = 'You Win!';
            } else {
              result = 'You Lose!';
              win = false;
            }
            Timer(Duration(seconds: 1), () {
              if (win != null) {
                setState(() {
                  showResult = true;
                });
              }
            });

            setState(() {
              win;
            });
          },
          child: showResult
              ? Container(
                  height: 280,
                  child: Center(
                    child: Text(
                      result,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 56.0,
                      ),
                    ),
                  ),
                )
              : Stack(
                  alignment: Alignment.center,
                  children: [
                    Center(
                      child: Icon(
                        Icons.monetization_on_outlined,
                        size: 280,
                        color: win == null
                            ? Colors.yellow
                            : win!
                                ? Colors.green
                                : Colors.red,
                      ),
                    ),
                    AnimatedBuilder(
                      animation: _controller,
                      builder: (context, child) {
                        return Transform.translate(
                          offset: Offset(500 * _controller.value - 250, 0),
                          child: Center(
                            child: Icon(
                              Icons.monetization_on,
                              size: 250,
                              color: win == null
                                  ? Colors.yellow
                                  : win!
                                      ? Colors.green
                                      : Colors.red,
                            ),
                          ),
                        );
                      },
                    ),
                    /*SlideTransition(
                  position: _offsetAnimation,
                  child: Container(
                    child: Center(
                      child: Icon(
                        Icons.monetization_on,
                        size: 250,
                        color: Colors.yellow,
                      ),
                    ),
                  )),*/
                  ],
                ),
        ),
        bottomBar,
      ],
    );
  }

  Widget get bottomBar => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FloatingActionButton(
            heroTag: 'reset',
            child: Icon(Icons.cached),
            backgroundColor: accentColor,
            mini: true,
            onPressed: () => reset(),
          ),
        ],
      );

  void reset() {
    _controller.reset();
    _controller.repeat();
    setState(() {
      win = null;
      showResult = false;
      result = '';
    });
  }

  void initializeAnimation() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 2200),
      vsync: this,
    )..repeat(reverse: false);

    myAnimationController = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _controller,
      // curve: Curves.elasticIn,
      curve: Curves.ease,
    ));
  }
}
