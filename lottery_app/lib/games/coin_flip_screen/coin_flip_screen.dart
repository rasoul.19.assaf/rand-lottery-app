/*
import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';

class HomePage2 extends StatefulWidget {
  @override
  State<HomePage2> createState() => _HomePage2State();
}

class _HomePage2State extends State<HomePage2> {
  _renderBg() {
    return Container(
      decoration: BoxDecoration(color: Colors.red),
    );
  }

  late FlipCardController _controller;

  @override
  void initState() {
    super.initState();
    _controller = FlipCardController();
  }

  _renderAppBar(context) {
    return MediaQuery.removePadding(
      context: context,
      removeBottom: true,
      child: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.green,
      ),
    );
  }

  _renderContent(context) {
    return Card(
      elevation: 0.0,
      margin: EdgeInsets.only(left: 32.0, right: 32.0, top: 20.0, bottom: 0.0),
      color: Color(0x00000000),
      child: FlipCard(
        controller: _controller,
        direction: FlipDirection.HORIZONTAL,
        speed: 500,
        onFlipDone: (status) {
          _controller.controller?.repeat();
          print(status);
        },
        front: Container(
          decoration: BoxDecoration(
            color: Color(0xFF006666),
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Front', style: Theme.of(context).textTheme.headline1),
              Text('Click here to flip back',
                  style: Theme.of(context).textTheme.bodyText1),
            ],
          ),
        ),
        back: Container(
          decoration: BoxDecoration(
            color: Color(0xFF006666),
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Back', style: Theme.of(context).textTheme.headline1),
              Text('Click here to flip front',
                  style: Theme.of(context).textTheme.bodyText1),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        _renderBg(),
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _renderAppBar(context),
            Expanded(
              flex: 4,
              child: _renderContent(context),
            ),
            Expanded(
              flex: 1,
              child: Container(),
            ),
          ],
        )
      ],
    );
  }
}
*/

import 'dart:math';

import 'package:flutter/material.dart';

class CoinFlipScreen extends StatefulWidget {
  @override
  State<CoinFlipScreen> createState() => _CoinFlipScreenState();
}

class _CoinFlipScreenState extends State<CoinFlipScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;
  AnimationStatus _animationStatus = AnimationStatus.dismissed;

  bool isBack = false;
  double counter = 0.5;
  bool isClicked = false;
  bool rightSelected = false;
  bool leftSelected = false;

  /**
   *
   */
  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 5));
    _animation = Tween<double>(end: 8, begin: 0).animate(
        CurvedAnimation(parent: _animationController, curve: Curves.ease))
      ..addListener(() {
        setState(() {
          if (_animation.value - counter > 0.5) {
            isBack = !isBack;
            counter += 0.5;
          }
        });
      })
      ..addStatusListener((status) {
        _animationStatus = status;
      });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          /*   Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 40,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 60,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(2),
                          bottomRight: Radius.circular(2),
                        ),
                      ),
                    ),
                    child: Text(
                      "طرة",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 32,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                  height: 60,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(2),
                          topLeft: Radius.circular(2),
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                      ),
                    ),
                    child: Text(
                      "نقش",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 32,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),*/
          SizedBox(
            height: 80,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Opacity(
                  opacity: isClicked ? 0.5 : 1,
                  child: Container(
                    height: 100,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: leftSelected ? Colors.white : Colors.blue,
                        onPrimary: leftSelected ? Colors.black : Colors.white,
                      ),
                      onPressed: () {
                        if (isClicked) {
                          return;
                        }
                        setState(() {
                          rightSelected = false;
                          leftSelected = true;
                        });
                      },
                      child: Text("يسار"),
                    ),
                  ),
                ),
              ),
              Container(
                // margin: EdgeInsets.only(top: 100),
                child: Transform(
                  alignment: FractionalOffset.center,
                  transform: Matrix4.identity()
                    ..setEntry(3, 2, 0.002)
                    ..rotateY(pi * _animation.value),
                  child: Center(
                    child: GestureDetector(
                      onTap: () {
                        if (!leftSelected && !rightSelected) {
                          return;
                        }
                        isClicked = true;
                        if (_animationStatus == AnimationStatus.completed) {
                          _animationController.reset();
                        }
                        _animationController.forward();
                      },
                      child: Container(
                        // color: Colors.blueAccent,
                        width: 200,
                        height: 200,
                        child: !isClicked
                            ? Icon(
                                Icons.radio_button_checked,
                                color: Colors.yellow,
                                size: 200,
                              )
                            : const Icon(
                                Icons.arrow_circle_left,
                                color: Colors.yellow,
                                size: 200,
                              ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Opacity(
                  opacity: isClicked ? 0.5 : 1,
                  child: Container(
                    height: 100,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: rightSelected ? Colors.white : Colors.blue,
                        onPrimary: rightSelected ? Colors.black : Colors.white,
                      ),
                      onPressed: () {
                        if (isClicked) {
                          return;
                        }
                        setState(() {
                          rightSelected = true;
                          leftSelected = false;
                        });
                      },
                      child: Text("يمين"),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
/*

class MyHomePage extends StatefulWidget {
  MyHomePage({required this.title});

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late bool _displayFront;
  late bool _flipXAxis;

  @override
  void initState() {
    super.initState();
    _displayFront = true;
    _flipXAxis = true;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        constraints: BoxConstraints.tight(Size.square(200.0)),
        child: _buildFlipAnimation(),
      ),
    );
  }

  Widget __buildLayout(
      {required Key key,
      required String faceName,
      required Color backgroundColor}) {
    return Container(
      key: key,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(20.0),
        color: backgroundColor,
      ),
      child: Center(
        child: Text(faceName.substring(0, 1), style: TextStyle(fontSize: 80.0)),
      ),
    );
  }

  Widget _buildFront() {
    return __buildLayout(
      key: ValueKey(true),
      backgroundColor: Colors.red,
      faceName: "F",
    );
  }

  Widget _buildRear() {
    return __buildLayout(
      key: ValueKey(false),
      backgroundColor: Colors.red.shade700,
      faceName: "R",
    );
  }

  Widget _buildFlipAnimation() {
    return GestureDetector(
      onTap: () {
        setState(() {
          _displayFront = !_displayFront;
        });
      },
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 1000),
        transitionBuilder: __transitionBuilder,
        layoutBuilder: (widget, list) => Stack(children: [widget!, ...list]),
        child: _displayFront ? _buildFront() : _buildRear(),
        switchInCurve: Curves.ease,
        switchOutCurve: Curves.ease.flipped,
      ),
    );
  }

  Widget __transitionBuilder(Widget widget, Animation<double> animation) {
    final rotateAnim = Tween<double>(begin: 0, end: 10).animate(animation);
    return AnimatedBuilder(
      animation: rotateAnim,
      child: widget,
      builder: (context, widget) {
        final isUnder = (ValueKey(_displayFront) != widget!.key);
        final value =
            isUnder ? min(rotateAnim.value, pi / 2) : rotateAnim.value;
        return Transform(
          transform: Matrix4.rotationY(value),
          child: widget,
          alignment: Alignment.center,
        );
      },
    );
  }
}
*/
