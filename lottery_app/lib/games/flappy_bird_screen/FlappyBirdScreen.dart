import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lottery_app/games/flappy_bird_screen/barriers.dart';
import 'package:lottery_app/games/flappy_bird_screen/bird.dart';

class FlappyBirdScreen extends StatefulWidget {
  const FlappyBirdScreen({Key? key}) : super(key: key);

  @override
  State<FlappyBirdScreen> createState() => _FlappyBirdScreenState();
}

class _FlappyBirdScreenState extends State<FlappyBirdScreen> {
  // bird vars
  static double birdY = 0;
  double initialHeight = birdY;
  double height = 0;
  double time = 0;
  double gravity = -4.9;
  double velocity = 3.5;
  double birdHeight = 0.1;
  double birdWidth = 0.1;

  // game settings
  bool gameStarted = false;

  // barriers vars
  static List<double> barrierX = [2, 2 + 1.5];
  static double barrierWidth = 0.5;
  List<List<double>> barrierHeight = [
    // [Top height, bottom height]
    [0.6, 0.4],
    [0.4, 0.6],
  ];

  // static double barrierXone = 1;
  // double barrierXtwo = barrierXone + 1.5;

  int highscore = 0;
  int score = 0;

  bool scoreAdded = false;

  double timeDelta = 0.05;

  @override
  void initState() {
    setState(() {
      scoreAdded = false;
      score = 0;
      birdY = 0;
      gameStarted = false;
      time = 0;
      initialHeight = birdY;
      barrierX = [2, 2 + 1.5];
    });
  }

  void jump() {
    setState(() {
      time = 0;
      initialHeight = birdY;
    });
  }

  Timer _timer = Timer(Duration(milliseconds: 1), () {});

  void startGame() {
    gameStarted = true;
    _timer = Timer.periodic(Duration(milliseconds: 60), (timer) {
      height = gravity * time * time + velocity * time;

      setState(() {
        birdY = initialHeight - height;
      });
      //Check if bird is dead
      if (birdIsDead()) {
        timer.cancel();
        gameStarted = false;
        _showDialogue();
      }
      moveMap();
      timeDelta += 0.0001;
      time += 0.05;
    });
  }

  void resetGame() {
    _timer.cancel();
    Navigator.pop(context);
    setState(() {
      timeDelta = 0.05;
      scoreAdded = false;
      score = 0;
      birdY = 0;
      gameStarted = false;
      time = 0;
      initialHeight = birdY;
      barrierX = [2, 2 + 1.5];
    });
  }

  void moveMap() {
    for (int i = 0; i < barrierX.length; i++) {
      // keep barriers moving
      setState(() {
        barrierX[i] -= timeDelta;
      });
      // if barrier exits the left part of the scree, keep it looping
      if (barrierX[i] < -1.5) {
        barrierX[i] += 3;
        scoreAdded = false;
        setRandomHeights(i);
      }
    }
    if ((barrierX[0] < 0 || barrierX[1] < 0) && !scoreAdded) {
      setState(() {
        score += 1;
      });
      scoreAdded = true;
    }
  }

  bool birdIsDead() {
    // Check if bird is hitting the top or the bottom of the screen
    if (birdY > 1 || birdY < -1) {
      return true;
    }
    // Check if bird is hitting a barrier
    for (int i = 0; i < barrierX.length; i++) {
      if (barrierX[i] <= birdWidth &&
          barrierX[i] + barrierWidth >= -birdWidth &&
          (birdY <= -1 + barrierHeight[i][0] ||
              birdY + birdHeight >= 1 - barrierHeight[i][1])) {
        return true;
      }
    }

    return false;
  }

  void _showDialogue() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.brown,
            title: const Text(
              "GAME OVER",
              style: TextStyle(color: Colors.white),
            ),
            content: Text(
              "Score: " + score.toString(),
              style: const TextStyle(
                color: Colors.white,
              ),
            ),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    if (score > highscore) {
                      highscore = score;
                    }
                    resetGame();
                  },
                  child: const Text(
                    "PLAY AGAIN",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ))
            ],
          );
        });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: gameStarted ? jump : startGame,
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
                flex: 5,
                child: Container(
                  color: Colors.blue,
                  child: Center(
                    child: Stack(
                      children: [
                        MyBird(
                          birdY: birdY,
                          birdWidth: birdWidth,
                          birdHeight: birdHeight,
                        ),
                        Container(
                          alignment: const Alignment(
                            0,
                            -0.3,
                          ),
                          child: gameStarted
                              ? const Text(" ")
                              : const Text(
                                  "T A P  TO  P L A Y",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),
                        ),
                        MyBarrier(
                          isThisBottomBarrier: false,
                          barrierX: barrierX[0],
                          barrierWidth: barrierWidth,
                          barrierHeight: barrierHeight[0][0],
                        ),
                        MyBarrier(
                          isThisBottomBarrier: true,
                          barrierWidth: barrierWidth,
                          barrierX: barrierX[0],
                          barrierHeight: barrierHeight[0][1],
                        ),
                        MyBarrier(
                          isThisBottomBarrier: false,
                          barrierX: barrierX[1],
                          barrierWidth: barrierWidth,
                          barrierHeight: barrierHeight[1][0],
                        ),
                        MyBarrier(
                          isThisBottomBarrier: true,
                          barrierWidth: barrierWidth,
                          barrierX: barrierX[1],
                          barrierHeight: barrierHeight[1][1],
                        ),
                      ],
                    ),
                  ),
                )),
            Container(
              height: 15,
              color: Colors.green,
            ),
            Expanded(
              flex: 1,
              child: Container(
                color: Colors.brown,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "SCORE",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          score.toString(),
                          style: TextStyle(color: Colors.white, fontSize: 35),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "BEST",
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          highscore.toString(),
                          style: const TextStyle(
                              color: Colors.white, fontSize: 35),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void setRandomHeights(int i) {
    Random rand = Random();
    double split = rand.nextDouble();
    split = max(split, 0.2);
    barrierHeight[i] = [split, 1 - split];
  }
}
