import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lottery_app/games/tic_tac_toe/themes.dart';

class RandomNumberScreen extends StatefulWidget {
  @override
  State<RandomNumberScreen> createState() => _RandomNumberScreenState();
}

class _RandomNumberScreenState extends State<RandomNumberScreen> {
  int number = 0;
  bool? stopped = false;
  int randomNumber = 0;
  final randomGenerator = Random();
  Timer? timer;

  void generateNumber() {
    if (stopped!) {
      return;
    }
    randomNumber = randomGenerator.nextInt(10);
    setState(() {
      number = randomNumber;
    });
  }

  void start() {
    timer = Timer.periodic(Duration(milliseconds: 20), (thisTimer) {
      if (stopped!) {
        thisTimer.cancel();
      }
      generateNumber();
    });
  }

  @override
  void initState() {
    start();
    generateNumber();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blue,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        children: [
          GestureDetector(
            onTap: () {
              if (!stopped!) {
                stopped = !stopped!;
              }
            },
            child: Center(
              child: Container(
                color: Colors.blue,
                child: Text(
                  "$number",
                  style: TextStyle(fontSize: 150, backgroundColor: Colors.blue),
                ),
              ),
            ),
          ),
          bottomBar,

          /* Container(
            margin: EdgeInsets.symmetric(horizontal: 40),
            width: double.infinity,
            height: 65,
            child: ElevatedButton(
              onPressed: () {},
              child: Text(
                "اختر",
                style: TextStyle(color: Colors.black, fontSize: 24),
              ),
              style: ElevatedButton.styleFrom(
                primary: Colors.yellow,
              ),
            ),
          )*/
        ],
      ),
    );
  }

  Widget get bottomBar => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FloatingActionButton(
              heroTag: 'reset',
              child: Icon(Icons.cached),
              backgroundColor: accentColor,
              mini: true,
              onPressed: () {
                if (stopped!) {
                  stopped = false;
                  start();
                }
              }),
        ],
      );
}
