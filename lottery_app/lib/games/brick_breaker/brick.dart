import 'package:flutter/material.dart';

class MyBrick extends StatelessWidget {
  final brickHeight;
  final brickWidth;
  final brickX;
  final brickY;
  final bool brickBroken;

  const MyBrick(
      {Key? key,
      this.brickHeight,
      this.brickWidth,
      this.brickX,
      this.brickY,
      required this.brickBroken})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return brickBroken
        ? Container()
        : Container(
            alignment:
                Alignment((2 * brickX + brickWidth) / (2 - brickWidth), brickY),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Container(
                height: MediaQuery.of(context).size.height * brickHeight / 2,
                width: MediaQuery.of(context).size.width * brickWidth / 2,
                color: Colors.deepPurple,
              ),
            ),
          );
  }
}
