import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:lottery_app/games/brick_breaker/ball.dart';
import 'package:lottery_app/games/brick_breaker/brick.dart';
import 'package:lottery_app/games/brick_breaker/coverscreen.dart';
import 'package:lottery_app/games/brick_breaker/gameoverscreen.dart';
import 'package:lottery_app/games/brick_breaker/player.dart';

class BrickBreaker extends StatefulWidget {
  const BrickBreaker({Key? key}) : super(key: key);

  @override
  State<BrickBreaker> createState() => _BrickBreakerState();
}

enum direction { UP, DOWN, LEFT, RIGHT }

class _BrickBreakerState extends State<BrickBreaker> {
  // ball variables
  double ballX = 0;
  double ballY = 0;
  double ballXIncrements = 0.02;
  double ballYIncrements = 0.01;
  var ballYDirection = direction.DOWN;
  var ballXDirection = direction.LEFT;

  Timer _timer = Timer(Duration(), () {});

  // bricks variables
  static int numberOfBricksInRow = 3;
  static double wallGap = 0.5 *
      (2 -
          numberOfBricksInRow * brickWidth -
          (numberOfBricksInRow - 1) * brickGap);
  static double firstBrickX = -1 + wallGap;
  static double firstBrickY = -0.9;
  static double brickWidth = 0.4; // out of 2
  static double brickHeight = 0.05; // out of 2
  static double brickGap = 0.01;

  List MyBricks = [
    // [X, Y, broken = true / false]
    [firstBrickX + 0 * (brickWidth + brickGap), firstBrickY, false],
    [firstBrickX + 1 * (brickWidth + brickGap), firstBrickY, false],
    [firstBrickX + 2 * (brickWidth + brickGap), firstBrickY, false],
  ];

  // Game Settings
  bool hasGameStarted = false;
  bool isGameOver = false;

  // Player variables
  double playerX = -0.2;
  double playerWidth = 0.4; // out of 2

  @override
  void dispose() {
    _timer.cancel();
  }

  // start game
  void startGame() {
    log("started");
    hasGameStarted = true;
    _timer = Timer.periodic(Duration(milliseconds: 10), (timer) {
      // update direction
      updateDirection();

      // move ball
      moveBall();

      // Check if player is dead
      if (isPlayerDead()) {
        _timer.cancel();
        isGameOver = true;
      }

      // Check for broken bricks
      checkForBrokenBricks();
    });
  }

  void checkForBrokenBricks() {
    // checks for when ball is inside the brick (aka hits brick)
    for (int i = 0; i < MyBricks.length; i++) {
      if (ballX >= MyBricks[i][0] &&
          ballX <= MyBricks[i][0] + brickWidth &&
          ballY <= MyBricks[i][1] + brickHeight &&
          MyBricks[i][2] == false) {
        print("Broken found");
        setState(() {
          MyBricks[i][2] = true;

          // since brick is broken, update direction of ball based on which side if the brick it hit
          // to do this, calculate the distance of the ball from each of the 4 sides.
          // the smallest distance is the side the ball has it

          double leftSideDist = (MyBricks[i][0] - ballX).abs();
          double rightSideDist = (MyBricks[i][0] + brickWidth - ballX).abs();
          double topSideDist = (MyBricks[i][1] - ballY).abs();
          double bottomSideDist = (MyBricks[i][1] + brickHeight - ballY).abs();

          String min =
              findMin(leftSideDist, rightSideDist, topSideDist, bottomSideDist);

          switch (min) {
            case 'bottom':
              // if ball hit bottom side of brick then
              ballYDirection = direction.DOWN;
              break;
            case 'top':
              // if ball hit top side of brick then
              ballYDirection = direction.UP;
              break;
            case 'left':
              // if ball hit left side of brick then
              ballXDirection = direction.LEFT;
              break;

            case 'right':
              // if ball hit right side of brick then
              ballXDirection = direction.RIGHT;
              break;
          }
        });
      }
    }
  }

  // is player dead
  bool isPlayerDead() {
    // player dies if ball reaches the bottom of the screen
    if (ballY >= 1) {
      return true;
    }
    return false;
  }

  // update direction of the ball
  void updateDirection() {
    setState(() {
      // ball goes up when it hits player
      if (ballY >= 0.9 && ballX >= playerX && ballX <= playerX + playerWidth) {
        ballYDirection = direction.UP;
      }
      // ball goes down when it hits the top of the screen
      else if (ballY <= -1) {
        ballYDirection = direction.DOWN;
      }
      // ball goes left when it hits the right of the screen
      if (ballX >= 1) {
        ballXDirection = direction.LEFT;
      }
      // ball goes left when it hits the left of the screen
      else if (ballX <= -1) {
        ballXDirection = direction.RIGHT;
      }
    });
  }

  // move ball
  void moveBall() {
    setState(() {
      // move horizontally
      if (ballXDirection == direction.LEFT) {
        ballX -= ballXIncrements;
      } else if (ballXDirection == direction.RIGHT) {
        ballX += ballXIncrements;
      }

      // move vertically
      if (ballYDirection == direction.DOWN) {
        ballY += ballYIncrements;
      } else if (ballYDirection == direction.UP) {
        ballY -= ballYIncrements;
      }
    });
  }

  void resetY() {
    _timer.cancel();
    setState(() {
      ballY = 0;
    });
  }

  // move player left
  void moveLeft() {
    setState(() {
      // only move left if moving left doesn't move player off the screen
      if (!(playerX - 0.2 < -1)) {
        playerX -= 0.2;
      }
    });
  }

  // move player right
  void moveRight() {
    setState(() {
      // only move right if moving right doesn't move player off the screen
      if (!(playerX + playerWidth >= 1)) {
        playerX += 0.2;
      }
    });
  }

  // reset game back to initial values when user hits play again
  void resetGame() {
    playerX = -0.2;
    ballX = 0;
    ballY = 0;
    isGameOver = false;
    hasGameStarted = false;
    MyBricks = [
      // [X, Y, broken = true / false]
      [firstBrickX + 0 * (brickWidth + brickGap), firstBrickY, false],
      [firstBrickX + 1 * (brickWidth + brickGap), firstBrickY, false],
      [firstBrickX + 2 * (brickWidth + brickGap), firstBrickY, false],
    ];
    setState(() {});
  }

  /*RawKeyboardListener
  *    focusNode: FocusNode(),
      autofocus: true,
      onKey: (event) {
        if (event.isKeyPressed(LogicalKeyboardKey.arrowLeft)) {
          moveLeft();
        } else if (event.isKeyPressed(LogicalKeyboardKey.arrowRight)) {
          moveRight();
        }
      },*/

  bool startMoving = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: !hasGameStarted ? startGame : () {},
      onHorizontalDragUpdate: (details) {
        setState(() {
          playerX += details.delta.dx;
          print(playerX);
        });
      },
      child: Scaffold(
        backgroundColor: Colors.deepPurple[100],
        body: Center(
          child: Stack(
            children: [
              // Tap to play
              CoverScreen(
                hasGameStarted: hasGameStarted,
                isGameOver: isGameOver,
              ),

              // game over screen
              GameOverScreen(
                isGameOver: isGameOver,
                function: resetGame,
              ),

              // ball
              MyBall(
                ballX: ballX,
                ballY: ballY,
                hasGameStarted: hasGameStarted,
                isGameOver: isGameOver,
              ),

              // player
              MyPlayer(
                playerX: playerX,
                playerWidth: playerWidth,
              ),

              // Bricks
              MyBrick(
                brickX: MyBricks[0][0],
                brickY: MyBricks[0][1],
                brickWidth: brickWidth,
                brickHeight: brickHeight,
                brickBroken: MyBricks[0][2],
              ),
              MyBrick(
                brickX: MyBricks[1][0],
                brickY: MyBricks[1][1],
                brickWidth: brickWidth,
                brickHeight: brickHeight,
                brickBroken: MyBricks[1][2],
              ),
              MyBrick(
                brickX: MyBricks[2][0],
                brickY: MyBricks[2][1],
                brickWidth: brickWidth,
                brickHeight: brickHeight,
                brickBroken: MyBricks[2][2],
              ),
            ],
          ),
        ),
      ),
    );
  }

  // returns the smallest side
  String findMin(double a, double b, double c, double d) {
    List<double> myList = [
      a,
      b,
      c,
      d,
    ];
    double currentMin = a;
    for (int i = 0; i < myList.length; i++) {
      if (myList[i] < currentMin) {
        currentMin = myList[i];
      }
    }

    if ((currentMin - a).abs() < 0.01) {
      return 'left';
    } else if (((currentMin - b).abs() < 0.01)) {
      return 'right';
    } else if (((currentMin - c).abs() < 0.01)) {
      return 'top';
    } else if (((currentMin - d).abs() < 0.01)) {
      return 'bottom';
    }
    return '';
  }
}
