import 'package:flutter/material.dart';
import 'package:lottery_app/games/tetris/widgets/block.dart';
import 'package:lottery_app/games/tetris/widgets/game.dart';
import 'package:lottery_app/games/tetris/widgets/next_block.dart';
import 'package:lottery_app/games/tetris/widgets/score_bar.dart';

class TetrisScreen extends StatefulWidget {
  const TetrisScreen({Key? key}) : super(key: key);

  @override
  State<TetrisScreen> createState() => _TetrisScreenState();
}

class _TetrisScreenState extends State<TetrisScreen> {
  GlobalKey<GameState> _keyGame = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TETRIS'),
        centerTitle: true,
        backgroundColor: Colors.indigo,
      ),
      backgroundColor: Colors.indigo,
      body: SafeArea(
        child: Column(
          children: [
            ScoreBar(),
            Expanded(
                child: Center(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 10, 5, 10),
                      child: Game(
                        key: _keyGame,
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(5, 10, 10, 10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          NextBlock(),
                          SizedBox(height: 30),
                          ElevatedButton(
                            onPressed: () {
                              setState(() {
                                if (_keyGame.currentState != null) {
                                  dynamic state = _keyGame.currentState;
                                  state.isPlaying
                                      ? state.endGame()
                                      : state.startGame();
                                }
                              });
                            },
                            child: _keyGame.currentState != null &&
                                    _keyGame.currentState!.isPlaying
                                ? Text('End')
                                : Text('Start'),
                            style: ElevatedButton.styleFrom(
                              textStyle: TextStyle(
                                fontSize: 18,
                                color: Colors.grey[700],
                              ),
                            ),
                          ),
                          Spacer(),
                          GestureDetector(
                            onTap: () {
                              _keyGame.currentState!.setState(() {
                                _keyGame.currentState!.action =
                                    BlockMovement.ROTATE_CLOCKWISE;
                              });
                            },
                            child: const Icon(
                              Icons.rotate_right_rounded,
                              size: 70,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          GestureDetector(
                            onTap: () {
                              _keyGame.currentState!.dropBlock();
                            },
                            child: const Icon(
                              Icons.arrow_drop_down_circle_outlined,
                              size: 70,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }
}
