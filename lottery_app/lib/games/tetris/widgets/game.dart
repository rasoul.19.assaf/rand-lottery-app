import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lottery_app/games/tetris/widgets/block.dart';
import 'package:lottery_app/games/tetris/widgets/sub_block.dart';

const BLOCKS_X = 10;
const BLOCKS_Y = 20;
const GAME_AREA_BORDER_WIDTH = 2.0;
const REFRESH_RATE = 300;
const SUB_BLOCK_EDGE_WIDTH = 2.0;

enum Collision {
  LANDED,
  LANDED_BLOCK,
  HIT_WALL,
  HIT_BLOCK,
  NONE,
}

class Game extends StatefulWidget {
  const Game({Key? key}) : super(key: key);

  @override
  State<Game> createState() => GameState();
}

class GameState extends State<Game> {
  bool isGameOver = false;
  late double subBlockWidth;
  Duration duration = Duration(milliseconds: REFRESH_RATE);
  late Timer timer;
  GlobalKey _keyGameArea = GlobalKey();

  Block? block;
  bool isPlaying = false;

  List<SubBlock> oldSubBlocks = [];
  BlockMovement? action;
  int score = 0;

  bool isPressed = false;

  Block? getNewBlock() {
    Random random = Random();
    int blockType = random.nextInt(7);
    int orientationIndex = random.nextInt(4);

    switch (blockType) {
      case 0:
        return IBlock(orientationIndex);
      case 1:
        return JBlock(orientationIndex);
      case 2:
        return LBlock(orientationIndex);
      case 3:
        return OBlock(orientationIndex);
      case 4:
        return TBlock(orientationIndex);
      case 5:
        return SBlock(orientationIndex);
      case 6:
        return ZBlock(orientationIndex);
      default:
        return null;
    }
  }

  void startGame() {
    isGameOver = false;
    isPlaying = true;
    oldSubBlocks = [];
    score = 0;
    RenderBox? renderBoxGame =
        _keyGameArea.currentContext!.findRenderObject() as RenderBox?;
    if (renderBoxGame == null) return;
    subBlockWidth =
        (renderBoxGame.size.width - GAME_AREA_BORDER_WIDTH * 2) / BLOCKS_X;

    block = getNewBlock(); // TODO: For testing
    timer = Timer.periodic(Duration(milliseconds: 300), onPlay);
  }

  Future<void> onPlay(Timer timer) async {
    var status = Collision.NONE;

    setState(() {
      if (action != null) {
        if (!checkOnEdge()) {
          block!.move(action!);
        }
      }

      // check rotation on edges
      if (block!.x + block!.width > BLOCKS_X || block!.x < 0) {
        reverseAction();
      }

      // Reverse action if the block hits other blocks
      for (var oldSubBlock in oldSubBlocks) {
        for (var subBlock in block!.subBlocks) {
          var x = block!.x + subBlock.x;
          var y = block!.y + subBlock.y;
          if (x == oldSubBlock.x && y == oldSubBlock.y) {
            reverseAction();
          }
        }
      }

      if (!checkAtBottom()) {
        if (!checkAboveBlock()) {
          if (action == null) {
            block!.move(BlockMovement.DOWN);
          }
        } else {
          status = Collision.LANDED_BLOCK;
        }
      } else {
        status = Collision.LANDED;
      }
      if (status == Collision.LANDED_BLOCK && block!.y < 0) {
        isGameOver = true;
        endGame();
      } else if (status == Collision.LANDED ||
          status == Collision.LANDED_BLOCK) {
        block!.subBlocks.forEach((subBlock) {
          subBlock.x += block!.x;
          subBlock.y += block!.y;
          oldSubBlocks.add(subBlock);
        });
        block = getNewBlock();
      }
    });

    setState(() {
      action = null;
      updateScore();
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void updateScore() {
    var combo = 1;
    Map<int, int> rows = Map();
    List<int> rowsToBeRemoved = [];

    // Count number of sub-blocks in each row
    oldSubBlocks.forEach((subBlock) {
      rows.update(subBlock.y, (value) => ++value, ifAbsent: () => 1);
    });

    // Add score if a full row is found
    rows.forEach((rowNum, count) {
      if (count == BLOCKS_X) {
        // Provider.of<Data>(context).addScore(combo++); // TODO
        rowsToBeRemoved.add(rowNum);
      }
    });

    if (rowsToBeRemoved.length > 0) {
      removeRows(rowsToBeRemoved);
    }
  }

  void removeRows(List<int> rowsToBeRemoved) {
    rowsToBeRemoved.sort();
    rowsToBeRemoved.forEach((rowNum) {
      oldSubBlocks.removeWhere((subBlock) => subBlock.y == rowNum);
      oldSubBlocks.forEach((subBlock) {
        if (subBlock.y < rowNum) {
          ++subBlock.y;
        }
      });
    });
  }

  bool checkAtBottom() {
    if (block != null) return block!.y + block!.height == BLOCKS_Y;
    return false;
  }

  bool checkOnEdge() {
    if (block != null) {
      return (action == BlockMovement.LEFT && block!.x <= 0) ||
          (action == BlockMovement.RIGHT &&
              block!.x + block!.width >= BLOCKS_X);
    }
    return false;
  }

  bool checkAboveBlock() {
    for (var oldSubBlock in oldSubBlocks) {
      if (block != null) {
        for (var subBlock in block!.subBlocks) {
          var x = block!.x + subBlock.x;
          var y = block!.y + subBlock.y;
          if (x == oldSubBlock.x && y + 1 == oldSubBlock.y) return true;
        }
      }
    }
    return false;
  }

  void endGame() {
    setState(() {
      isPlaying = false;
    });
    timer.cancel();
  }

  Positioned getPositionedSquareContainer(Color color, int x, int y) {
    return Positioned(
      left: x * subBlockWidth,
      top: y * subBlockWidth,
      child: Container(
        width: subBlockWidth - SUB_BLOCK_EDGE_WIDTH,
        height: subBlockWidth - SUB_BLOCK_EDGE_WIDTH,
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(
            const Radius.circular(3.0),
          ),
        ),
      ),
    );
  }

  Widget drawBlocks() {
    List<Widget> subBlocks = [];

    if (block != null) {
      // Current Block
      for (var subBlock in block!.subBlocks) {
        subBlocks.add(
          getPositionedSquareContainer(
              subBlock.color, subBlock.x + block!.x, subBlock.y + block!.y),
        );
      }
    }

    for (var oldSubBlock in oldSubBlocks) {
      subBlocks.add(getPositionedSquareContainer(
          oldSubBlock.color, oldSubBlock.x, oldSubBlock.y));
    }
    if (isGameOver) {
      subBlocks.add(getGameOverRect());
    }
    return Stack(
      children: subBlocks,
    );
  }

  Widget getGameOverRect() {
    return Positioned(
        child: Container(
          padding: EdgeInsets.all(18),
          // width: subBlockWidth * 8.0,
          // height: subBlockWidth * 3.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.red,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: Text(
            'Game Over',
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
        ),
        left: subBlockWidth * 1.0,
        top: subBlockWidth * 6.0);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (details) async {
        isPressed = true;
        do {
          print('long pressing'); // for testing
          print('enter');
          final RenderBox box = context.findRenderObject() as RenderBox;
          final localOffset = box.globalToLocal(details.globalPosition);
          final x = localOffset.dx;
          print(box.size);
          print(x);
          // if x is less than halfway across the screen and user is not on first page
          if (x < box.size.width / 2) {
            print("left");
            action = BlockMovement.LEFT;

            //   _pageController.previousPage(...)
          } else {
            print('right');
            action = BlockMovement.RIGHT;

            // Assume the user tapped on the other half of the screen and check they are not on the last page
            //  _pageController.nextPage(...)
          }
          await Future.delayed(Duration(milliseconds: 300));
        } while (isPressed);
      },
      onHorizontalDragEnd: (details) {
        setState(() => isPressed = false);
      },
      onTapUp: (details) {
        setState(() => isPressed = false);
      },
      /* onLongPressDown: (details) {
        print('enter');
        final RenderBox box = context.findRenderObject() as RenderBox;
        final localOffset = box.globalToLocal(details.globalPosition);
        final x = localOffset.dx;
        print(box.size);
        print(x);
        // if x is less than halfway across the screen and user is not on first page
        if (x < box.size.width / 2) {
          print("left");
          action = BlockMovement.LEFT;

          //   _pageController.previousPage(...)
        } else {
          print('right');
          action = BlockMovement.RIGHT;

          // Assume the user tapped on the other half of the screen and check they are not on the last page
          //  _pageController.nextPage(...)
        }
      },*/
      child: AspectRatio(
        aspectRatio: BLOCKS_X / BLOCKS_Y,
        child: Container(
          key: _keyGameArea,
          decoration: BoxDecoration(
            color: Colors.indigo[800],
            border: Border.all(
              width: GAME_AREA_BORDER_WIDTH,
              color: Colors.indigoAccent,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(10.0)),
          ),
          child: drawBlocks(),
        ),
      ),
    );
  }

  void reverseAction() {
    switch (action) {
      case BlockMovement.LEFT:
        block!.move(BlockMovement.RIGHT);
        break;
      case BlockMovement.RIGHT:
        block!.move(BlockMovement.LEFT);
        break;
      case BlockMovement.ROTATE_CLOCKWISE:
        block!.move(BlockMovement.ROTATE_COUNTER_CLOCKWISE);
        break;
      default:
        break;
    }
  }

  Future<void> dropBlock() async {
    while (true) {
      if (!checkAtBottom()) {
        if (!checkAboveBlock()) {
          block!.move(BlockMovement.DOWN);
        } else {
          await Future.delayed(Duration(milliseconds: 200));

          return;
        }
      } else {
        await Future.delayed(Duration(milliseconds: 200));

        return;
      }
    }
  }
}
