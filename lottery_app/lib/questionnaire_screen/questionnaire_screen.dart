import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lottery_app/data.dart';
import 'package:lottery_app/questionnaire_screen/widgets/radio_choice_container.dart';

class QuestionaireScreen extends StatefulWidget {
  @override
  _QuestionaireScreenState createState() => _QuestionaireScreenState();
}

class _QuestionaireScreenState extends State<QuestionaireScreen> {
  int val = -1;
  double progress = 1;
  int questionId = 0;
  List userAnswers = [
    0,
    0,
    0,
  ];

  Color backgroundColor = Colors.blue;

  @override
  void initState() {
    increaseProgress();
  }

  @override
  Widget build(BuildContext context) {
    // print(MediaQuery.of(context).size);
    return SafeArea(
      child: Scaffold(
          backgroundColor: backgroundColor,
          body: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: 140,
                    // margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(18),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Text("00:00:14"),
                            SizedBox(
                              width: 10,
                            ),
                            Text("2000"),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: LinearProgressIndicator(
                                value: 0.01,
                                backgroundColor: Colors.yellow,
                                color: Colors.grey,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.card_giftcard,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text("00:00:14"),
                            SizedBox(
                              width: 10,
                            ),
                            Text("2000"),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: LinearProgressIndicator(
                                value: 0.05,
                                backgroundColor: Colors.yellow,
                                color: Colors.grey,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.card_giftcard,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text("00:00:14"),
                            SizedBox(
                              width: 10,
                            ),
                            Text("2000"),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: LinearProgressIndicator(
                                value: 0.05,
                                backgroundColor: Colors.yellow,
                                color: Colors.grey,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.card_giftcard,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                // false
                //     // ? RandomNumberScreen()
                //     ? CoinFlipScreen()
                //     :
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          getTimerProgress(),
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 5, right: 5, bottom: 17),
                      child: LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        color: Colors.blueAccent,
                        minHeight: 5,
                        value: progress,
                      ),
                    ),
                    Text(
                      questions[questionId],
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    RadioChoiceContainer(
                      answerState: userAnswers[0],
                      value: 0,
                      groupValue: val,
                      onChanged: (Object? value) {
                        val = value! as int;
                        setState(() {
                          val;
                        });
                      },
                      text: choices[questionId][0],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    RadioChoiceContainer(
                      answerState: userAnswers[1],
                      value: 1,
                      groupValue: val,
                      onChanged: (Object? value) {
                        val = value! as int;
                        setState(() {
                          val;
                        });
                      },
                      text: choices[questionId][1],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    RadioChoiceContainer(
                      answerState: userAnswers[2],
                      value: 2,
                      groupValue: val,
                      onChanged: (Object? value) {
                        val = value! as int;
                        setState(() {
                          val;
                        });
                      },
                      text: choices[questionId][2],
                    ),
                    SizedBox(
                      height: 45,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 40),
                      width: double.infinity,
                      height: 65,
                      child: ElevatedButton(
                        onPressed: () {
                          userAnswers[0] = 0;
                          userAnswers[1] = 0;
                          userAnswers[2] = 0;

                          if (checkAnswer(val, questionId)) {
                            setState(() {
                              userAnswers[val] = 1;
                            });
                          } else {
                            setState(() {
                              userAnswers[answers[questionId]] = 1;
                              userAnswers[val] = 2;
                            });
                          }
                          progress = 1;
                          Timer(Duration(seconds: 3), () {
                            setState(() {
                              questionId += 1;
                              questionId %= questions.length;
                              userAnswers = [0, 0, 0];
                              val = -1;
                            });
                          });
                          /**/
                        },
                        child: Text(
                          "اختر",
                          style: TextStyle(color: Colors.black, fontSize: 24),
                        ),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.yellow,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )),
    );
  }

  void increaseProgress() {
    Timer timer = Timer(Duration(milliseconds: 100), () {
      if (progress > 0) {
        setState(() {
          progress -= 0.01;
        });
        increaseProgress();
      } else {
        userAnswers[answers[questionId]] = 1;
        setState(() {
          Timer(Duration(seconds: 3), () {
            progress = 1;
            questionId += 1;
            questionId %= questions.length;
            userAnswers = [0, 0, 0];
            val = -1;
            increaseProgress();
          });
        });
      }
    });
  }

  bool checkAnswer(int val, int questionId) {
    return val == answers[questionId];
  }

  String getTimerProgress() {
    String seconds = "${(progress * 10 + 1).toInt()}";
    // print((progress * 100).toInt());
    // String milliseconds = ((progress * 100).toInt() % 10).toString();
    return "$seconds";
  }
}
