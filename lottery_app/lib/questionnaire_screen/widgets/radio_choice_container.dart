import 'package:flutter/material.dart';

class RadioChoiceContainer extends StatelessWidget {
  final int groupValue;
  final int value;
  final void Function(Object?) onChanged;
  final String text;
  final int answerState;

  const RadioChoiceContainer(
      {Key? key,
      required this.groupValue,
      required this.onChanged,
      required this.value,
      required this.text,
      required this.answerState})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      height: 45,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Visibility(
            visible: answerState != 0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: answerState == 1
                  ? Icon(
                      Icons.check_circle,
                      color: Colors.green,
                    )
                  : Icon(
                      Icons.cancel,
                      color: Colors.red,
                    ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                text,
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontSize: 24,
                ),
              ),
              Radio(
                groupValue: groupValue,
                onChanged: onChanged,
                value: value,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
